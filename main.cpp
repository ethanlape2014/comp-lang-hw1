#include <regex>
#include <vector>
#include <fstream>
#include <iostream>
#include <iterator>
#include <unordered_map>

enum class ZpmVariableType {
    Int, Str, None,
};

enum class ZpmError {
    VariableNotExists, WrongTypes, DivideByZero, UnkownCommand
};

class ZpmVariable {
public:
    ZpmVariableType type;

    int int_val;
    std::string str_val;

    ZpmVariable() : type{ ZpmVariableType::None }, int_val{}, str_val{} {
	
    }

    ZpmVariable(int val) : type{ ZpmVariableType::Int }, int_val{ val } {
	
    }

    ZpmVariable(std::string val) : type { ZpmVariableType::Str }, str_val{ std::move(val) } {
	
    }
};

// variables used by zpm
namespace {
    const auto null_variable = ZpmVariable{};

    auto current_line = 1ull;
    auto variables = std::unordered_map<std::string, ZpmVariable>{};
}

auto split_string(const std::string& str, const std::string& regex) {
    const auto r = std::regex{ regex };
    const auto first = std::sregex_token_iterator{ std::begin(str), std::end(str), r, -1 };
    const auto last = std::sregex_token_iterator{};

    return std::vector<std::string>{ first, last };
}

template <typename Iterator>
auto extract_for(Iterator begin, Iterator end) {
    auto counter = 0;
    auto ret = std::vector<std::string>{};

    for (auto i = begin; i < end; ++i) {
	ret.push_back(*i);
	
	if (*i == "FOR") {
	    counter++;
	} else if (*i == "ENDFOR") {
	    if (counter == 1) {
		break;
	    } else {
		counter--;
	    }
	}
    }

    return ret;
}

template <typename Iterator>
auto extract_expressions(Iterator begin, Iterator end) {
    auto ret = std::vector<std::vector<std::string>>{ 1, std::vector<std::string>{} };
    auto vector_count = 0;

    for (auto i = begin; i < end; ++i) {
	if (*i == "FOR") {
	    ret.push_back(extract_for(i, end));
	    vector_count++;
	} else if (*i == ";") {
	    ret.push_back(std::vector<std::string>{});
	    vector_count++;
	} else {
	    ret[vector_count].push_back(*i);
	}
    }

    return ret;
}

auto is_variable(const std::string& name) {
    return variables.find(name) != variables.end();
}

auto to_variable(const std::string& token) {
    if (is_variable(token)) {
	return variables[token];
    }

    if (token.front() == '"' && token.back() == '"') {
	return ZpmVariable(token.substr(1, token.size() - 2));
    }

    for (auto c : token) {
	if (!std::isdigit(c)) {
	    return null_variable;
	}
    }

    return ZpmVariable{ std::stoi(token) };
}

template <typename Iterator>
void parse_print(Iterator begin, Iterator end) {
    const auto token = begin[1];
    const auto variable = to_variable(token);

    if (variable.type == ZpmVariableType::Int) {
	std::cout << variable.int_val << std::endl;
    } else if (variable.type == ZpmVariableType::Str) {
	std::cout << variable.str_val << std::endl;
    } else {
	std::cout << "zpm: error '" << token << "' does not exist." << std::endl;
	throw ZpmError::VariableNotExists;
    }
}

template <typename Iterator>
void parse_assign(Iterator begin, Iterator end) {
    const auto to = tokens[0];
    const auto from = to_variable(tokens[2]);

    if (from.type == ZpmVariableType::None) {
	std::cout << "zpm: error, '" << tokens[2] << "' does not name a valid variable or expression." << std::endl;
	throw ZpmError::VariableNotExists;
    }

    variables[to] = from;
}

template <typename Iterator>
void parse_add(Iterator begin, Iterator end) {
    const auto lhs = tokens[0];
    const auto rhs = tokens[2];
    
    auto to = to_variable(lhs);
    const auto from = to_variable(rhs);

    if (from.type == ZpmVariableType::None) {
	std::cout << "zpm: error, '" << rhs << "' does not name a valid variable or expression." << std::endl;
	throw ZpmError::VariableNotExists;
    }

    if (to.type != from.type) {
	std::cout << "zpm: error, '" << lhs << "' and '" << rhs << "' are not the same type, they cannot be added together." << std::endl;
	throw ZpmError::WrongTypes;
    }

    if (to.type == ZpmVariableType::Int) {
	to.int_val += from.int_val;
    } else {
	to.str_val += from.str_val;
    }

    variables[lhs] = to;
}

void parse_subtract(const std::vector<std::string>& tokens) {
    const auto lhs = tokens[0];
    const auto rhs = tokens[2];
    
    auto to = to_variable(lhs);
    const auto from = to_variable(rhs);

    if (from.type == ZpmVariableType::None) {
	std::cout << "zpm: error, '" << rhs << "' does not name a valid variable or expression." << std::endl;
	throw ZpmError::VariableNotExists;
    }

    if (to.type != ZpmVariableType::Int) {
	std::cout << "zpm: error, '" << lhs << "' is not an integer, you cannot subtract it." << std::endl;
	throw ZpmError::WrongTypes;
    }

    if (to.type != from.type) {
	std::cout << "zpm: error, '" << lhs << "' and '" << rhs << "' are not the same type, they cannot be added together." << std::endl;
	throw ZpmError::WrongTypes;
    }

    to.int_val -= from.int_val;

    variables[lhs] = to;
}

void parse_multiply(const std::vector<std::string>& tokens) {
    const auto lhs = tokens[0];
    const auto rhs = tokens[2];
    
    auto to = to_variable(lhs);
    const auto from = to_variable(rhs);

    if (from.type == ZpmVariableType::None) {
	std::cout << "zpm: error, '" << rhs << "' does not name a valid variable or expression." << std::endl;
	throw ZpmError::VariableNotExists;
    }

    if (to.type != ZpmVariableType::Int) {
	std::cout << "zpm: error, '" << lhs << "' is not an integer, you cannot multiply it." << std::endl;
	throw ZpmError::WrongTypes;
    }

    if (to.type != from.type) {
	std::cout << "zpm: error, '" << lhs << "' and '" << rhs << "' are not the same type, they cannot be added together." << std::endl;
	throw ZpmError::WrongTypes;
    }

    to.int_val *= from.int_val;

    variables[lhs] = to;
}

void parse_divide(const std::vector<std::string>& tokens) {
    const auto lhs = tokens[0];
    const auto rhs = tokens[2];
    
    auto to = to_variable(lhs);
    const auto from = to_variable(rhs);

    if (from.type == ZpmVariableType::None) {
	std::cout << "zpm: error, '" << rhs << "' does not name a valid variable or expression." << std::endl;
	throw ZpmError::VariableNotExists;
    }

    if (to.type != ZpmVariableType::Int) {
	std::cout << "zpm: error, '" << lhs << "' is not an integer, you cannot divide it." << std::endl;
	throw ZpmError::WrongTypes;
    }

    if (to.type != from.type) {
	std::cout << "zpm: error, '" << lhs << "' and '" << rhs << "' are not the same type, they cannot be added together." << std::endl;
	throw ZpmError::WrongTypes;
    }

    if (from.int_val == 0) {
	std::cout << "zpm: error, '" << rhs << "' is zero, cannot divide by zero." << std::endl;
	throw ZpmError::DivideByZero;
    }

    to.int_val /= from.int_val;

    variables[lhs] = to;
}

template <typename Iterator>
void parse_expression(Iterator begin, Iterator end) {
    if (begin[0] == "FOR") {
	const auto count = std::stoi(begin[1]);

	for (auto i = 0; i < count; i++) {
	    parse_expression(begin + 2, end - 1);
	}
    } else if (begin[0] == "PRINT") {
	parse_print(tokens);
    } else if (begin[1] == "=") {
	parse_assign(tokens);	    
    } else if (begin[1] == "+=") {
	parse_add(tokens);
    } else if (begin[1] == "-=") {
	parse_subtract(tokens);
    } else if (begin[1] == "*=") {
	parse_multiply(tokens);
    } else if (begin[1] == "/=") {
	parse_divide(tokens);
    } else {
	std::cout << "zpm: error, unkown command" << std::endl;
	throw ZpmError::UnkownCommand;
    }
}

template <typename VectorIterator>
void parse_file_line(VectorIterator begin, VectorIterator end) {
    const auto expressions = extract_expressions(begin, end);

    for (const auto& expression : expressions) {
	parse_expression(std::begin(expression), std::end(expression));
    }
}

auto main(int argc, char** argv) -> int {
    if (argc < 2) {
	std::cout << "zpm: error, no source file specified." << std::endl;
	return EXIT_FAILURE;
    }

    const auto args = std::vector<std::string>{ argv, argv + argc };
    const auto filename = args[1];

    auto file = std::fstream{ filename };
    auto line = std::string{};

    while (std::getline(file, line)) {
	const auto tokens = split_string(line, "\\s+(?=(?:[^\\\"]*[\\\"][^\\\"]*[\\\"])*[^\\\"]*$)");

	std::cout << "found tokens:" << std::endl;
	for (auto t : tokens) {
	    std::cout << t << ", ";
	}
	std::cout << std::endl;
	continue;

	try {
	    parse_file_line(std::begin(tokens), std::end(tokens));	    
	} catch (ZpmError e) {
	    std::cout << "zpm: aborting." << std::endl;
	    return EXIT_FAILURE;
	}
    }
}
