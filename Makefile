GXX	= g++
FLAGS	= -std=c++14 -O3
SOURCE	= main.cpp
TARGET	= zpm

all:
	$(GXX) $(FLAGS) $(SOURCE) -o $(TARGET)

clean:
	rm $(TARGET)
